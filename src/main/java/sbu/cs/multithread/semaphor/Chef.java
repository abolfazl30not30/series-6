package sbu.cs.multithread.semaphor;

import java.util.concurrent.Semaphore;

public class Chef extends Thread {
    private Semaphore semaphore;
    public Chef(String name,Semaphore semaphore) {
        super(name);
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            semaphore.acquire();
            System.out.println("get permit :"+Thread.currentThread().getName());
            for (int i = 0; i < 10; i++) {
                Source.getSource();
                try {
                    System.out.println(Thread.currentThread().getName());
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
