package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

public class Calculator implements Runnable{
    private int counter;
    private Sum sum;
    private final int scale = 1000;
    public Calculator(int counter, Sum sum){
        this.counter = counter;
        this.sum = sum;
    }

    public void run(){

        BigDecimal face = new BigDecimal(1).setScale(scale,RoundingMode.HALF_DOWN);
        BigDecimal two = new BigDecimal(2).setScale(scale,RoundingMode.HALF_DOWN);
        BigDecimal denominator = new BigDecimal(1).setScale(scale,RoundingMode.HALF_DOWN);

        for (int i = 1; i <= this.counter; i++) {
            BigDecimal tmp = new BigDecimal(i).setScale(scale, RoundingMode.HALF_DOWN);
            face = face.multiply(tmp);
        }

        face = face.pow(2);
        two = two.pow(this.counter + 1);
        face = face.multiply(two);

        for(int j = 1  ; j <= (this.counter * 2) + 1 ; j++){
            BigDecimal tmp2 = new BigDecimal(j).setScale(scale, RoundingMode.HALF_DOWN);
            denominator = denominator.multiply(tmp2);
        }
        face = face.divide(denominator,RoundingMode.HALF_DOWN);

        this.sum.add(face);

    }
}
