package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.*;

public class PICalculator {

    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */

    /*
        Please read this before judge my code :D
        I assume that this program will run in a 4-core cpu.
     */
    private ExecutorService eService = Executors.newFixedThreadPool(16);


    public String calculate(int floatingPoint) {
        try {
            Executor executor = new Executor();
            BigDecimal answer = new BigDecimal(0).setScale(1000, RoundingMode.HALF_DOWN);
            answer = executor.computing();
            String strAnswer = answer.toString();
            strAnswer = strAnswer.substring(0, floatingPoint+2);
            return strAnswer;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}

