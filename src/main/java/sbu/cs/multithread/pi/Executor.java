package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Executor {
    private static final ExecutorService executor = Executors.newFixedThreadPool(16);

     public BigDecimal computing() throws InterruptedException {
        Sum sum = new Sum();

        long st = System.currentTimeMillis();
        for(int i = 0; i < 10000; i++){
            executor.submit(new Calculator(i,sum));
        }
        executor.shutdown();
        executor.awaitTermination(10, TimeUnit.SECONDS);

        return sum.getSum();
    }
}
