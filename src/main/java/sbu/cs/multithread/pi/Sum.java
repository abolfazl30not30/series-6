package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Sum {
    private BigDecimal sum = new BigDecimal(0);
    public Sum(){
        this.sum.setScale(1000,RoundingMode.HALF_DOWN);
    }

    synchronized void add(BigDecimal num){
        sum = sum.add(num);
    }

    public BigDecimal getSum(){
        return sum;
    }
}
