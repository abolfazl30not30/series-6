package sbu.cs.exception;

import sbu.cs.exception.NewException.BadInput;
import sbu.cs.exception.NewException.NotImplementedCommand;
import sbu.cs.exception.NewException.UnrecognizedCommand;

import java.util.ArrayList;
import java.util.List;

public class Reader {

    /**
     * declare 2 Exception class. 1 for UnrecognizedCommand and 1 for NotImplementedCommand
     * iterate on function inputs and check for commands and throw exception when needed.
     *
     * @param args
     */
    public void readTwitterCommands(List<String> args){
        for(String i : args){
            if(Implemented(i)){
                continue;
            }
            else {
                if (notImplemented(i)){
                    throw new NotImplementedCommand();
                }
                else {
                    throw new UnrecognizedCommand();
                }
            }
        }
    }

    private boolean Implemented(String st){
        List<String> ImplementedCommands = new ArrayList<>();
        ImplementedCommands = Util.getImplementedCommands();
        for(String command : ImplementedCommands){
            if(st.equals(command)){
                return true;
            }
        }
        return false;
    }

    private boolean notImplemented(String st){
        List<String> notImplementedCommands = new ArrayList<>();
        notImplementedCommands = Util.getNotImplementedCommands();
        for(String command : notImplementedCommands){
            if(st.equals(command)){
                return true;
            }
        }
        return false;
    }

    /**
     * function inputs are String but odd positions must be integer parsable
     * a valid input is like -> "ap", "2", "beheshti", "3992", "20"
     * throw BadInput exception when the string is not parsable.
     *
     * @param args
     */
    public void read(String...args){
        for(int i = 0; i < args.length; i++){
            if(i % 2 != 0){
                try{
                    int n = Integer.parseInt(args[i]);
                }
                catch (Exception e){
                    throw new BadInput();
                }
            }
        }
    }
}
