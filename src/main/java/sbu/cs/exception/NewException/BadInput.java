package sbu.cs.exception.NewException;

import sbu.cs.exception.ApException;

public class BadInput extends ApException {
    public BadInput(){
        super("Bad input Exception");
    }
}
