package sbu.cs.exception.NewException;
import sbu.cs.exception.ApException;

public class NotImplementedCommand extends ApException {
    public NotImplementedCommand(){
        super("Command is not implemented!");
    }
}
