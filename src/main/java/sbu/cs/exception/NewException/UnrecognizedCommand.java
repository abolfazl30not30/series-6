package sbu.cs.exception.NewException;

import sbu.cs.exception.ApException;

public class UnrecognizedCommand extends ApException {
    public UnrecognizedCommand(){
        super("This command is unrecognizable!");
    }
}
