package sbu.cs.exception;

public class ApException extends RuntimeException {
    private String massage;
    public ApException(String massage){
        super(massage);
        this.massage = massage;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }
}
